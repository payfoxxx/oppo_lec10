#include "pch.h"
#include <iostream>

using namespace std;

double my_pow(double a, unsigned int k) {
	double res = a;
	for (int i = 1; i < k; i++) {
		res *= a;
	}
	return res;
}

int main()
{
	setlocale(LC_ALL, "rus");
	double a;
	unsigned int k;
	cout << "Введите число: ";
	cin >> a;
	cout << "\nВведите степень: ";
	cin >> k;
	double aink;
	aink = my_pow(a, k);
	cout << "\nРезультат: " << aink;
}


